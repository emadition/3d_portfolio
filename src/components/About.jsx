import React from 'react';
import { Tilt } from 'react-tilt';
import { motion } from 'framer-motion';
import { fadeIn, textVariant } from '../utils/motion';
import { styles } from '../styles';
import { services } from '../constants';
import { SectionWrapper } from '../hoc';

const ServiceCard = ({ index, title, icon }) => {
    return (
        <Tilt className='xs:w-[250px] w-full'>
            <motion.div
                variants={fadeIn('right', 'spring', 0.5 * index, 0.75)}
                className='w-full green-pink-gradient shadow-card p-[1px] rounded-[20px]'
            >
                <div
                    options={{ max: 45, scale: 1, speed: 450 }}
                    className='bg-tertiary px-12 py-5 rounded-[20px] flex flex-col justify-evenly items-center min-h-[280px]'
                >
                    <img src={icon} className='w-16 h-16 object-contain' />
                    <h3 className='text-center text-white text-[20px] font-bold'>{title}</h3>
                </div>
            </motion.div>
        </Tilt>
    );
};

const About = () => {
    return (
        <>
            <motion.div variants={textVariant()}>
                <p className={styles.sectionSubText}>Introduction</p>
                <h1 className={styles.sectionHeadText}>Overview.</h1>
            </motion.div>
            <motion.p
                variants={fadeIn('', '', 0.1, 1)}
                className='mt-5 text-secondary text-[17px] leading-[30px] max-w-3xl'
            >
                I'm a self tought developer with 2 plus years of experience in reactjs, typescript and redux. I am
                passionate about learning new technologies.
            </motion.p>

            <div className='mt-20 flex flex-wrap gap-10'>
                {services.map((service, index) => {
                    return <ServiceCard key={service.title} index={index} {...service} />;
                })}
            </div>
        </>
    );
};

export default SectionWrapper(About, 'about');
